Welcome to libqc++'s documentation!
===================================

Contents:

.. toctree::
   :maxdepth: 2

   applications
   dev/index

Indices and tables
==================

* :ref:`genindex`
* :ref:`search`
